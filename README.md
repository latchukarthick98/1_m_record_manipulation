# 1 Million Profile Operations

This project focuses on generating 1 Million profiles and search a specific record from a JSON file.

## File specification
### generate.py
This file generates 1 Milion profiles using Faker Library and writes them to a file named "1M-profile.json"

### search.py
This file implements an iterative search mechanism to find a record from JSON file.

## Installation Instructions
pip install -r requirements.txt

## Execution Instructions
1. python generate.py
2. python search.py

## TO-DOs
- [x] Generate 1 Milion profiles.
- [x] Search for a specific record.
- [ ]  Delete and item.