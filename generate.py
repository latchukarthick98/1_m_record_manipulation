from json import dumps
from faker import Faker
import collections
import json

# A function to generate fake record of a person using
#   'Faker Library'
def fake_person_generator(length, fake):
    for x in range(length):
        yield {'last_name': fake.last_name(),
               'first_name': fake.first_name(),
               'street_address': fake.street_address(),
               'email': fake.email(),
               'index': x,
               'isDeleted': False}

filename = '1M-profile'
length   = 1000000 # 1M Records
fake     = Faker() # An object initialized for Faker
fpg = fake_person_generator(length, fake)

# This Loop generates 1 Millon Records of Fake person profile
# and write them into a JSON file.
with open('%s.json' % filename, 'w') as output:
    output.write('[') # To maintain a JSON List structure.
    for person in fpg:
        json.dump(person, output)
         #Check  for the Last record, as including comma at the end would malform the JSON Strucure.
        if person["index"] != (length-1):
            output.write(',')
    output.write(']') # End of a List.
print("Done.") #Prompt on done Writing the file.