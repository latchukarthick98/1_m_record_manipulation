import json
import time


start = time.time()
jsondata = open("1M-profile.json",'r').read() # Open JSON File in read mode.

profile = json.loads(jsondata) #Load the JSON content for further processing.
end = time.time()
print('Dataset Loaded in ',(end - start))

def search(search_id):

        # Iterative search
        for entity in profile:
                if entity["index"] == search_id:
                        # Return When found
                        return entity


index = int(input('Enter an Index to search: ')) #Prompt for a record to search.
fetch_start = time.time()
#Print the time taken for search
print( search(index) )
fetch_end = time.time()

print('Searched in ', (fetch_end - fetch_start))
